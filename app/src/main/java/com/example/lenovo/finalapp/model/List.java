package com.example.lenovo.finalapp.model;

import com.example.lenovo.finalapp.Weather;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {
    /*@SerializedName("dt")
    @Expose
    public Integer dt;*/
    @SerializedName("main")
    @Expose
    public MainModel main;
    @SerializedName("weather")
    @Expose
    public java.util.List<MainModel> weather = null;
    @SerializedName("clouds")
    @Expose
    public Clouds clouds;
    /*@SerializedName("wind")
    @Expose
    public Wind wind;*/
    @SerializedName("sys")
    @Expose
    public Sys sys;
    @SerializedName("dt_txt")
    @Expose
    public String dtTxt;
}
