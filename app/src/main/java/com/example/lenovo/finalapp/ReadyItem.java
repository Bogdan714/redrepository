package com.example.lenovo.finalapp;

import android.os.Parcel;
import android.os.Parcelable;

public class ReadyItem implements Parcelable {

    public int id;
    public String icon;
    public String state;
    public String temperature;
    public String humidity;
    public String pressure;

    public String sunrise;
    public String sunset;
    public String visibility;
    public String cityName;
    public String dateTxt;

    public ReadyItem(int id, String icon, String state, String temperature, String humidity, String pressure, String sunrise, String sunset, String visibility, String cityName, String dateTxt) {
        this.id = id;
        this.icon = icon;
        this.state = state;
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.visibility = visibility;
        this.cityName = cityName;
        this.dateTxt = dateTxt;
    }

    public static Creator<ReadyItem> getCREATOR() {
        return CREATOR;
    }

    protected ReadyItem(Parcel in) {
        id = in.readInt();
        state  = in.readString();
        temperature = in.readString();
        humidity = in.readString();
        pressure = in.readString();
        sunrise = in.readString();
        sunset = in.readString();
        visibility = in.readString();
        cityName = in.readString();
        dateTxt = in.readString();
    }

    public static final Parcelable.Creator<ReadyItem> CREATOR = new Parcelable.Creator<ReadyItem>() {
        @Override
        public ReadyItem createFromParcel(Parcel in) {
            return new ReadyItem(in);
        }

        @Override
        public ReadyItem[] newArray(int size) {
            return new ReadyItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
    //
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(icon);
        dest.writeString(state);
        dest.writeString(temperature);
        dest.writeString(humidity);
        dest.writeString(pressure);
        dest.writeString(sunrise);
        dest.writeString(sunset);
        dest.writeString(visibility);
        dest.writeString(cityName);
        dest.writeString(dateTxt);
    }
}
