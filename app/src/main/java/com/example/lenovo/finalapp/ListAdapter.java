package com.example.lenovo.finalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private List<ReadyItem> readyItems;

    public ListAdapter(Context context, List<ReadyItem> readyItems) {
        this.context = context;
        this.readyItems = readyItems;
    }

    public ListAdapter(Context context) {
        this.context = context;
        this.readyItems = new ArrayList<>();
    }

    public void update() {
        notifyDataSetChanged();
    }

    public void clear() {
        readyItems.clear();
    }

    public void addAll(List<ReadyItem> countryList) {
        readyItems.addAll(countryList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return readyItems.size();
    }

    @Override
    public ReadyItem getItem(int position) {
        return readyItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return readyItems.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.list_node, parent, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }



        holder.generalDescription.setText(getItem(position).state);
        holder.realTemperature.setText(getItem(position).temperature);
        holder.humidity.setText(getItem(position).humidity);
        holder.pressure.setText(getItem(position).pressure);
        // holder.icon.setImageResource(getItem(position).icon);
        return rowView;
    }

    static class ViewHolder {

        @BindView(R.id.general_description)
        TextView generalDescription;

        @BindView(R.id.real_temperature)
        TextView realTemperature;

        @BindView(R.id.humidity)
        TextView humidity;

        @BindView(R.id.pressure)
        TextView pressure;

        @BindView(R.id.icon)
        ImageView icon;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
