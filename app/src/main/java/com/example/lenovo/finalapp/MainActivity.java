package com.example.lenovo.finalapp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lenovo.finalapp.model.ArrayOfDate;
import com.example.lenovo.finalapp.model.List;
import com.example.lenovo.finalapp.model.MainModel;
import com.example.lenovo.finalapp.model.MyWeather;
import com.example.lenovo.finalapp.model.Weather2;

import java.util.ArrayList;

import butterknife.OnItemClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    java.util.List<Weather> weathers;
    final java.util.List<ReadyItem> listOfItems = new ArrayList<>();
    ListAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sendRequest();
        adapter = new ListAdapter(this);
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

    }

    private void sendRequest(){
        String nameCity = "Kharkiv";
        String nameType = "json";
        String apiKey = "492df05db708aaa62a619adffb2de7d2";
        Retrofit.getSubRegion(nameCity, nameType,apiKey, new Callback<MyWeather>() {
            @Override
            public void success(MyWeather mainModel, Response response) {
                MyWeather mainModell = mainModel;
                 java.util.List<ArrayOfDate> list = mainModell.list;

                for (int i = 0; i < 5; i++) {
                    listOfItems.add(new ReadyItem(list.get(i).weather.get(0).id,
                            list.get(i).weather.get(0).icon,
                            list.get(i).weather.get(0).main,
                            list.get(i).main.temp.toString(),
                            list.get(i).main.humidity.toString(),
                            list.get(i).main.pressure.toString(),
                            "sys ",
                            "sys ",
                            "vis ",
                            mainModel.city.name,list.get(i).dtTxt));

                    //int id, String icon, String state, String temperature, String humidity, String pressure, String sunrise, String sunset, String visibility, String cityName, String dateTxt
                }

                adapter.addAll(listOfItems);

                 Float value = list.get(0).main.tempMin;
                 String icon = mainModell.list.get(0).weather.get(0).icon;
                 String firstIcon = "http://openweathermap.org/img/w/" + icon + ".png";


                Toast.makeText(MainActivity.this, value.toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position){
        //Intent intent = new Intent(this, )

    }

//    public void getFromDataBase(String cityName) {
//
//        final WeatherDataBase db = Room.databaseBuilder(getApplicationContext(), WeatherDataBase.class, "weather-database").build();
//        final String cityNam = cityName;
//
//        Observable.just(db)
//                .observeOn(Schedulers.io())
//                .subscribeOn(AndroidSchedulers.mainThread())
//                .map(new Func1<WeatherDataBase, List<Weather>>() {
//                    @Override
//                    public List<Weather> call(WeatherDataBase weatherDataBase) {
//
//                        List<Weather> weatherList = db.getWeatherDao().getCities(cityNam);
//
//                        return weatherList;
//                    }
//                })
//                .subscribe(weatherList->{
//                            weathers = weatherList;
//
//                });
//    }
//
//    public void methodForRetrofit(java.util.List<Weather> weatherList) {
//
//
//        final java.util.List<Weather> liiist = weatherList;
//
//        final WeatherDataBase db = Room.databaseBuilder(getApplicationContext(), WeatherDataBase.class, "weather-database").build();
//
//        Observable.just(db)
//                .observeOn(Schedulers.io())
//                .subscribeOn(AndroidSchedulers.mainThread())
//                .map(new Func1<WeatherDataBase, Void>() {
//                    @Override
//                    public Void call(WeatherDataBase weatherDataBase) {
//
//                        for (Weather weather : liiist) {
//                            db.getWeatherDao().insertAll(weather);
//                        }
//
//                        return null;
//                    }
//                })
//                .subscribe();
//    }
}
