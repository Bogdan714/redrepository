package com.example.lenovo.finalapp;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Weather{

    /*@PrimaryKey(autoGenerate = true)
    public int id;
    @SerializedName("main")
    public String state;
    @SerializedName("temp")
    public String temperature;
    public String humidity;
    public String pressure;

    public String sunrise;
    public String sunset;
    public String visibility;
    public String cityName;
    public String dateTxt;*/

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("main")
    @Expose
    public String main;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("icon")
    @Expose
    public String icon;

}
