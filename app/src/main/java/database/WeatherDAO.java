/*
package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.lenovo.finalapp.Weather;

import java.util.List;

@Dao
public interface WeatherDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Weather... weathers);

    @Query("SELECT * From weather")
    List<Weather> getAll();

    @Query("SELECT * From weather where cityName like :cityName")
    List<Weather> getCities(String cityName);

}
*/
